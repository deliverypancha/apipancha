<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "codigo_telefono".
 *
 * @property int $id
 * @property string $telefono
 * @property string $codigo
 * @property int $valido
 */
class CodigoTelefono extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'codigo_telefono';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono', 'codigo'], 'required'],
            [['valido'], 'integer'],
            [['telefono', 'codigo'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefono' => 'Telefono',
            'codigo' => 'Codigo',
            'valido' => 'Valido',
        ];
    }
}
