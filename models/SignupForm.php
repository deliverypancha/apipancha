<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
 
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $password_hash;
    public $access_token;
    public $rol;
    public $moto_id;
    public $email;
    public $telefono;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required', 'message' => 'No puede estar vacío este campo'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'El email ya existe.'],
            
            ['password_hash', 'required', 'message' => 'No puede estar vacío este campo'],
            ['password_hash', 'string', 'min' => 6],
            
            
            //[['status', 'created_at', 'update_at'], 'integer'],
            [['email', 'password_hash'], 'required',],
            [['password_hash'], 'string', 'max' => 255],

            ['telefono', 'unique', 'targetClass' => '\app\models\Moto', 'message' => 'El telefono ya existe.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password_hash' => 'Contraseña',
            'status' => 'Status',
        ];
    }
 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
 
        $user = new User();
        $user->setPassword($this->password_hash);
        $user->access_token = Yii::$app->security->generateRandomString();
        $user->rol = User::ROL_MOTO;
        $user->moto_id = $this->moto_id;
        $user->email = $this->email;
        $user->generateAuthKey();
        $user->generateAccessToken();
        $user->status = User::STATUS_ACTIVE;
        if (!$user->save()){
            throw new \Exception( ErrorsComponent::formatJustString($user->errors) );
            return null;
        }
        return $user;
    }
}