<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Signup form
 */
class Signup extends Model
{
    // /**
    //  * @var string funciona como username de usuario
    //  */
    // public $username;

    /**
     * @var string email de usaurio
     */
    public $email;

    /**
     * @var string password del ususario
     */
    public $password;
    
    /**
     * @var string password del ususario
     */
    public $tipo_login;

    /**
     * @var string
     * Variable para confirmar el password ingresado concide con el pasword del nuevo usuario
     */
    public $retypePassword;

    public $nombre_completo;
    public $telefono_celular;
    public $ciudad;
    public $player_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // ['username', 'filter', 'filter' => 'trim'],
            // ['username', 'required'],
            // ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
            // ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'El email ya existe.'],

            ['password', 'required'],
            ['password', 'string'],
            
            ['tipo_login', 'integer'],

            [['player_id'], 'string', 'max'=>300],

            ['retypePassword', 'required'],
            ['retypePassword', 'compare', 'compareAttribute' => 'password'],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->nombre_completo = $this->nombre_completo;
            $user->telefono_celular = $this->telefono_celular;
            $user->ciudad = $this->ciudad;
            $user->email = $this->email;
            $user->player_id = $this->player_id;
            $user->tipo_login = 1;
            $user->fecha = date('Y-m-d');
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateAccessToken();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            // $this->_user = User::findByUsername($this->username);
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
