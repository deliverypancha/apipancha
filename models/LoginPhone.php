<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Login form
 */
class LoginPhone extends Model
{
    /**
     * @var string variable que represetna telefono de un user
     */
    public $telefono;
    
    /**
     * @var string variable que represetna telefono de un user
     */
    public $rememberMe = true;

    
    /**
     * @var boolean|User Variable para obtener el usuario que es autentificado o false
     */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['telefono', 'number'],
        ];
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        return $this->getUser();
        if ($this->validate()) {
            return Yii::$app->getUser()->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByTelefono($this->telefono);
        }

        return $this->_user;
    }
}
