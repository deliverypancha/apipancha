<?php 
namespace app\controllers;

use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\Signup;
use app\models\User;
use app\models\Login;
use app\models\ChangePassword;
use app\models\AsignacionMoto;
use app\models\PedidoDeliveryDetalle;
use app\models\Pedidodelivery;
use app\models\Moto;
use app\models\Cliente;
use app\models\LoginPhone;
use app\models\SignupForm;
use app\models\CodigoTelefono;
use app\models\Sucursaldelivery;

use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\BadRequestHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UnprocessableEntityHttpException;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


use app\components\errors\ErrorsComponent;
use Carbon\Carbon;
/**
 * Moto.
 * 
 * una moto puede ver los psedidos asignados a el
 * , puede cambiar su foto de perfil, cambiar su contrasena, 
 * ver sus datos de perfil ocn la lista de pedidos que el ah 
 * realizado
 * 
 */
class MotoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'corsFilter'  => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
            'bearerAuth' => [
                'class' => HttpBearerAuth::className(),
                'except'=>[
                    'login',
                    'crear-clientes',
                    'login-phone',
                    'login-phone-confirm',
                ]
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => [
                            'index', 
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'login' => ['post'],
                    'pedidos' => ['get'],
                    'perfil' => ['get'],
                    'modificar-foto' => ['post'],
                    'ver-pedido' => ['get'],
                    'entregar-pedido' => ['post'],
                    'cambiar-password' => ['post'],
                    'crear-clientes' => ['get'],
                    'login-phone' => ['post'],
                    'login-phone-confirm' => ['post'],
                    'update' => ['post'],
                    'sucursales' => ['get'],
                ],
            ],
        ];
    }

    /**
     * retorna un array con los datos del usuario
     *
     * @return User
     */
    private function formatUser($id = null){
        $user = \Yii::$app->user->identity;
        if ($id){
            $user = User::findOne($id);
        }
        return [
            'id'=>$user->id,
            'rol'=>$user->rol,
            'email'=>$user->email,
            'moto_id'=>$user->moto->id,
            'nombre'=>$user->moto->nombre,
            'ciudad'=>$user->moto->ciudad,
            'sucursal_id'=>$user->moto->sucursal_id,
            'sucursal'=>$user->moto->sucursal,
            'telefono'=>$user->moto->telefono,
            'foto'=>$user->moto->strFoto,
            'estado'=>$user->moto->estado,
            'player_id'=>$user->moto->player_id,
        ];
    }

    /**
     * login.
     *
     * login opara usuarios moto
     * 
     * `Url`: /moto/login <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: none <br>
     * `body`: <br>
     *     {
     *         "email":"[value]",
     *         "password":"[value]"
     *         "player_id":"[value]"
     *     }
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "user": {
     *                 "id": 485,
     *                 "rol": 2,
     *                 "email": "ariana@gmail.com",
     *                 "moto_id": 6,
     *                 "nombre": "ariana grande motnenegro",
     *                 "ciudad": "california estados unidos",
     *                 "sucursal_id": 3,
     *                 "sucursal": {
     *                     "id": 3,
     *                     "nombre": "SUCURSAL QUILLACOLLO",
     *                     "ciudad": "fddddddddddddddd",
     *                     "detalle": "fdfds",
     *                     "estado": "Activo"
     *                 },
     *                 "telefono": "6789987888",
     *                 "foto": "http://delivery.ds:8000/uploads/motos/moto_6200625100654.jpg",
     *                 "estado": 0,
     *                 "player_id": null
     *             },
     *             "access_token": "DwB6MJaMy1MwDqmkKPf_Rb2_0j0AWggM"
     *         }
     *     }
     */
    public function actionLogin(){
        $request = \Yii::$app->request;
        $login = new Login();
        $login->email = $request->post('email');
        $login->password = $request->post('password');
        if ( $login->login() ){
            $login->user->moto->player_id = $request->post('player_id');
            $login->user->moto->save();
            return [
                'user'=>$this->formatUser($login->user->id),
                'access_token'=>$login->user->access_token
            ];
        }else{
            throw new UnauthorizedHttpException("El usuario no es valido");
        }
    }

    private function pedidos($moto, $estado = null){
        $today = date('Y-m-d');
        $pedidos = $moto->getAsignaciones()
            ->all();
        if ( $estado ){
            $pedidos = $moto->getAsignaciones()
                ->joinWith('pedido')
                ->where([
                    'pedido_delivery.estado'=>$estado,
                    'pedido_delivery.fecha_entrega'=>$today
                ])
                ->all();
        }
        return ArrayHelper::toArray($pedidos, [
            AsignacionMoto::className() => [
                'id'=>function($model){ return $model->pedido->id; },
                'fecha_entrega'=>function($model){ return $model->pedido->fecha_entrega; },
                'hora_entrega'=>function($model){ return $model->pedido->hora_entrega; },
                'fecha_despacho'=>function($model){ return $model->fecha_despacho; },
                'hora_despacho'=>function($model){ return $model->hora_despacho; },
                'fecha_entregado'=>function($model){ return $model->fecha_entrega; },
                'hora_entregado'=>function($model){ return $model->hora_entrega; },
                'cliente_id'=>function($model){ return $model->pedido->cliente_id; },
                'cliente'=>function($model){ 
                    return $model->pedido->cliente; 
                },
                'telefono'=>function($model){ return $model->pedido->telefono; },
                'zona'=>function($model){ return $model->pedido->zona; },
                'direccion'=>function($model){ return $model->pedido->direccion; },
                'latitude'=>function($model){ return $model->pedido->latitude; },
                'longitude'=>function($model){ return $model->pedido->longitude; },
                'zoom'=>function($model){ return $model->pedido->zoom; },
                'instrucciones'=>function($model){ return $model->pedido->instrucciones; },
                'precio_delivery_id'=>function($model){ return $model->pedido->precio_delivery_id; },
                'precio_delivery'=>function($model){ 
                    return $model->pedido->precioDelivery; 
                },
                'sucursal_delivery_id'=>function($model){ return $model->pedido->sucursal_delivery_id; },
                'sucursal_delivery'=>function($model){
                    return $model->pedido->sucursalDelivery; 
                },
                'tipo_pedido_id'=>function($model){ return $model->pedido->tipo_pedido_id; },
                'tipo_pedido'=>function($model){ 
                    return $model->pedido->tipoPedido; 
                },
                'estado'=>function($model){ return $model->pedido->estado; },
                'strEstado'=>function($model){ return $model->pedido->strEstado; },
                'url_mapa'=>function($model){ return $model->pedido->url_mapa; },
                'detalle'=>function($model){ 
                    return ArrayHelper::toArray($model->pedido->detalle, [
                        PedidoDeliveryDetalle::className() => [
                            'id'=>function($model){ return $model->id; },
                            'producto_id'=>function($model){ return $model->producto_id; },
                            'producto'=>function($model){ 
                                if (!$model->producto){
                                    return null;
                                }
                                return [
                                    'id'=>$model->producto->id,
                                    'producto'=>$model->producto->producto,
                                    'foto'=>$model->producto->strFoto,
                                    'detalle'=>$model->producto->detalle,
                                    'costo'=>$model->producto->costo,
                                    'estado'=>$model->producto->estado,
                                    'categoria_producto_id'=>$model->producto->categoria_producto_id,
                                    'categoria_producto'=>$model->producto->categoria,
                                ];
                            },
                            'subtotal'=>function($model){ return $model->subtotal; },
                            'cantidad'=>function($model){ return $model->cantidad; },
                            'observacion'=>function($model){ return $model->observacion; },
                        ],
                    ]);
                },
                'total'=>function($model){ return $model->pedido->total; },
            ],
        ]);
    }

    /**
     * Pedidos.
     *
     * Muestra la lista de pedidos que tiene la moto
     * 
     * `Url`: /moto/pedidos <br>
     * `method`: get <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: token <br>
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": [
     *             {
     *                 "id": 22,
     *                 "fecha_entrega": "2020-06-25",
     *                 "hora_entrega": "13:13:00",
     *                 "fecha_despacho": "2020-06-25",
     *                 "hora_despacho": "11:16:00",
     *                 "fecha_entregado": null,
     *                 "hora_entregado": null,
     *                 "cliente_id": 8,
     *                 "cliente": {
     *                     "id": 8,
     *                     "nit": "88799999",
     *                     "telefono": "78989989",
     *                     "zona": "villa pagador",
     *                     "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                     "razon_social": "sandra garcia huanaco",
     *                     "zoom": "18",
     *                     "foto": null,
     *                     "latitude": "-17.436701",
     *                     "longitude": "-66.1170413"
     *                 },
     *                 "telefono": "78989989",
     *                 "zona": "villa pagador",
     *                 "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                 "latitude": "-17.436701",
     *                 "longitude": "-66.1170413",
     *                 "zoom": "18",
     *                 "instrucciones": "esta en esquina frente punto viva",
     *                 "precio_delivery_id": 6,
     *                 "precio_delivery": {
     *                     "id": 6,
     *                     "nombre": "Costo zona (6)",
     *                     "descripcion": "",
     *                     "precio": "25",
     *                     "estado": "Activo"
     *                 },
     *                 "sucursal_delivery_id": 4,
     *                 "sucursal_delivery": {
     *                     "id": 4,
     *                     "nombre": "SUCURSAL SUD",
     *                     "ciudad": null,
     *                     "detalle": "",
     *                     "estado": "Activo"
     *                 },
     *                 "tipo_pedido_id": 3,
     *                 "tipo_pedido": {
     *                     "id": 3,
     *                     "nombre": "ENTREGA PROGRAMADA",
     *                     "descripcion": "",
     *                     "estado": "Activo"
     *                 },
     *                 "estado": 1,
     *                 "strEstado": "Asignado",
     *                 "url_mapa": "https://www.google.com/maps/place/17%C2%B026'12.1%22S+66%C2%B007'01.4%22W/@-17.4366997,-66.1175882,19z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d-17.436701!4d-66.1170413",
     *                 "detalle": [
     *                     {
     *                         "id": 20,
     *                         "producto_id": 8,
     *                         "producto": {
     *                             "id": 8,
     *                             "producto": "panchicono",
     *                             "foto": "http://delivery.ds:8000/uploads/productos/producto_8200624160337.jpg",
     *                             "detalle": "fdsfdsfds",
     *                             "costo": "23",
     *                             "estado": "Activo",
     *                             "categoria_producto_id": 3,
     *                             "categoria_producto": {
     *                                 "id": 3,
     *                                 "nombre": "Comida",
     *                                 "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                                 "estado": "Activo"
     *                             }
     *                         },
     *                         "subtotal": 46,
     *                         "cantidad": 2,
     *                         "observacion": null
     *                     }
     *                 ],
     *                 "total": 46
     *             },
     *             {
     *                 "id": 23,
     *                 "fecha_entrega": "2020-06-25",
     *                 "hora_entrega": "14:36:00",
     *                 "fecha_despacho": "2020-06-25",
     *                 "hora_despacho": "11:38:00",
     *                 "fecha_entregado": null,
     *                 "hora_entregado": null,
     *                 "cliente_id": 2,
     *                 "cliente": {
     *                     "id": 2,
     *                     "nit": "67868887",
     *                     "telefono": "75977665",
     *                     "zona": "villa cosmos altaocbba",
     *                     "direccion": "frente a la chacha de sespedabandonada",
     *                     "razon_social": "wendi marbell",
     *                     "zoom": "13",
     *                     "foto": null,
     *                     "latitude": "-17.395874795713354",
     *                     "longitude": "-66.14234375004844"
     *                 },
     *                 "telefono": "75977665",
     *                 "zona": "villa cosmos altaocbba",
     *                 "direccion": "frente a la chacha de sespedabandonada",
     *                 "latitude": "-17.395874795713354",
     *                 "longitude": "-66.14234375004844",
     *                 "zoom": "13",
     *                 "instrucciones": "ahora si no tarden malditos prros",
     *                 "precio_delivery_id": 4,
     *                 "precio_delivery": {
     *                     "id": 4,
     *                     "nombre": "Costo zona (4)",
     *                     "descripcion": "",
     *                     "precio": "15",
     *                     "estado": "Activo"
     *                 },
     *                 "sucursal_delivery_id": 4,
     *                 "sucursal_delivery": {
     *                     "id": 4,
     *                     "nombre": "SUCURSAL SUD",
     *                     "ciudad": null,
     *                     "detalle": "",
     *                     "estado": "Activo"
     *                 },
     *                 "tipo_pedido_id": 2,
     *                 "tipo_pedido": {
     *                     "id": 2,
     *                     "nombre": "ENTREGA INMEDIATA",
     *                     "descripcion": "",
     *                     "estado": "Activo"
     *                 },
     *                 "estado": 1,
     *                 "strEstado": "Asignado",
     *                 "url_mapa": "",
     *                 "detalle": [
     *                     {
     *                         "id": 21,
     *                         "producto_id": 1,
     *                         "producto": {
     *                             "id": 1,
     *                             "producto": "Panchita",
     *                             "foto": "http://delivery.ds:8000/uploads/productos/producto_1200523150852.jpg",
     *                             "detalle": "Arroz + Papa + 3 Presas",
     *                             "costo": "32",
     *                             "estado": "Activo",
     *                             "categoria_producto_id": 3,
     *                             "categoria_producto": {
     *                                 "id": 3,
     *                                 "nombre": "Comida",
     *                                 "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                                 "estado": "Activo"
     *                             }
     *                         },
     *                         "subtotal": 32,
     *                         "cantidad": 1,
     *                         "observacion": null
     *                     },
     *                     {
     *                         "id": 22,
     *                         "producto_id": 8,
     *                         "producto": {
     *                             "id": 8,
     *                             "producto": "panchicono",
     *                             "foto": "http://delivery.ds:8000/uploads/productos/producto_8200624160337.jpg",
     *                             "detalle": "fdsfdsfds",
     *                             "costo": "23",
     *                             "estado": "Activo",
     *                             "categoria_producto_id": 3,
     *                             "categoria_producto": {
     *                                 "id": 3,
     *                                 "nombre": "Comida",
     *                                 "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                                 "estado": "Activo"
     *                             }
     *                         },
     *                         "subtotal": 46,
     *                         "cantidad": 2,
     *                         "observacion": null
     *                     },
     *                     {
     *                         "id": 23,
     *                         "producto_id": 3,
     *                         "producto": {
     *                             "id": 3,
     *                             "producto": "2 Presas",
     *                             "foto": null,
     *                             "detalle": "2 Presas de pollo, 1 porcion de arroz, 1 porcion de papa frita y dos unidades de platanito.",
     *                             "costo": "25",
     *                             "estado": "Activo",
     *                             "categoria_producto_id": 3,
     *                             "categoria_producto": {
     *                                 "id": 3,
     *                                 "nombre": "Comida",
     *                                 "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                                 "estado": "Activo"
     *                             }
     *                         },
     *                         "subtotal": 50,
     *                         "cantidad": 2,
     *                         "observacion": null
     *                     }
     *                 ],
     *                 "total": 128
     *             }
     *         ]
     *     }
     */
    public function actionPedidos(){
        $user = \Yii::$app->user->identity;
        return $this->pedidos($user->moto, Pedidodelivery::ESTADO_ASIGNADO);
    }

    /**
     * Perfil.
     *
     * Ver los datos de mi perfil y mi lista de pedidos
     * 
     * `Url`: /moto/perfil <br>
     * `method`: get <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: token <br>
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "user": {
     *                 "id": 485,
     *                 "rol": 2,
     *                 "email": "ariana@gmail.com",
     *                 "moto_id": 6,
     *                 "nombre": "ariana grande motnenegro",
     *                 "ciudad": "california estados unidos",
     *                 "sucursal_id": 3,
     *                 "sucursal": {
     *                     "id": 3,
     *                     "nombre": "SUCURSAL QUILLACOLLO",
     *                     "ciudad": "fddddddddddddddd",
     *                     "detalle": "fdfds",
     *                     "estado": "Activo"
     *                 },
     *                 "telefono": "6789987888",
     *                 "foto": "http://api.delivery.ds:8000/uploads/motos/moto_485200625123000.jpg",
     *                 "estado": 0,
     *                 "player_id": null
     *             },
     *             "pedidos": [
     *                 {
     *                     "id": 22,
     *                     "fecha_entrega": "2020-06-25",
     *                     "hora_entrega": "13:13:00",
     *                     "fecha_despacho": "2020-06-25",
     *                     "hora_despacho": "11:16:00",
     *                     "fecha_entregado": null,
     *                     "hora_entregado": null,
     *                     "cliente_id": 8,
     *                     "cliente": {
     *                         "id": 8,
     *                         "nit": "88799999",
     *                         "telefono": "78989989",
     *                         "zona": "villa pagador",
     *                         "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                         "razon_social": "sandra garcia huanaco",
     *                         "zoom": "18",
     *                         "foto": null,
     *                         "latitude": "-17.436701",
     *                         "longitude": "-66.1170413"
     *                     },
     *                     "telefono": "78989989",
     *                     "zona": "villa pagador",
     *                     "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                     "latitude": "-17.436701",
     *                     "longitude": "-66.1170413",
     *                     "zoom": "18",
     *                     "instrucciones": "esta en esquina frente punto viva",
     *                     "precio_delivery_id": 6,
     *                     "precio_delivery": {
     *                         "id": 6,
     *                         "nombre": "Costo zona (6)",
     *                         "descripcion": "",
     *                         "precio": "25",
     *                         "estado": "Activo"
     *                     },
     *                     "sucursal_delivery_id": 4,
     *                     "sucursal_delivery": {
     *                         "id": 4,
     *                         "nombre": "SUCURSAL SUD",
     *                         "ciudad": null,
     *                         "detalle": "",
     *                         "estado": "Activo"
     *                     },
     *                     "tipo_pedido_id": 3,
     *                     "tipo_pedido": {
     *                         "id": 3,
     *                         "nombre": "ENTREGA PROGRAMADA",
     *                         "descripcion": "",
     *                         "estado": "Activo"
     *                     },
     *                     "estado": 2,
     *                     "strEstado": "Terminado",
     *                     "url_mapa": "https://www.google.com/maps/place/17%C2%B026'12.1%22S+66%C2%B007'01.4%22W/@-17.4366997,-66.1175882,19z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d-17.436701!4d-66.1170413",
     *                     "detalle": [
     *                         {
     *                             "id": 20,
     *                             "producto_id": 8,
     *                             "producto": {
     *                                 "id": 8,
     *                                 "producto": "panchicono",
     *                                 "foto": "http://delivery.ds:8000/uploads/productos/producto_8200624160337.jpg",
     *                                 "detalle": "fdsfdsfds",
     *                                 "costo": "23",
     *                                 "estado": "Activo",
     *                                 "categoria_producto_id": 3,
     *                                 "categoria_producto": {
     *                                     "id": 3,
     *                                     "nombre": "Comida",
     *                                     "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                                     "estado": "Activo"
     *                                 }
     *                             },
     *                             "subtotal": 46,
     *                             "cantidad": 2,
     *                             "observacion": null
     *                         }
     *                     ],
     *                     "total": 46
     *                 }
     *             ]
     *         }
     *     }
     */
    public function actionPerfil(){
        $user = \Yii::$app->user->identity;
        return [
            'user'=>$this->formatUser(),
            'pedidos'=>$this->pedidos($user->moto, Pedidodelivery::ESTADO_TERMINADO)
        ];
    }

    /**
     * Modifiar foto.
     *
     * modifica la foto de perfil del usuario moto
     * 
     * `Url`: /moto/modificar-foto <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: token <br>
     * `body`: <br>
     *     {
     *         "foto":"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/"
     *     }
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "id": 485,
     *             "rol": 2,
     *             "email": "ariana@gmail.com",
     *             "moto_id": 6,
     *             "nombre": "ariana grande motnenegro",
     *             "ciudad": "california estados unidos",
     *             "sucursal_id": 3,
     *             "sucursal": {
     *                 "id": 3,
     *                 "nombre": "SUCURSAL QUILLACOLLO",
     *                 "ciudad": "fddddddddddddddd",
     *                 "detalle": "fdfds",
     *                 "estado": "Activo"
     *             },
     *             "telefono": "6789987888",
     *             "foto": "http://api.delivery.ds:8000/uploads/motos/moto_485200625123000.jpg",
     *             "estado": 0,
     *             "player_id": null
     *         }
     *     }
     */
    public function actionModificarFoto(){
        $request = \Yii::$app->request;
        $user = \Yii::$app->user->identity;
        $transaction = $user->db->beginTransaction();
        try{
            if ( !$request->post('foto') ){
                throw new \Exception( 'La foto es requerida' );
            }
            if ( $request->post('foto') ){
                if ( $user->moto->foto != '' ){
                    //eliminando foto existente
                    $imageExist = \Yii::getAlias('@imagePath').\app\models\Moto::PATH.$user->moto->foto;
                    if ( file_exists($imageExist) ){
                        unlink($imageExist);
                    }
                }
                $user->moto->foto = 'moto_'.$user->id.date('ymdhis').'.jpg';
                //guardando la foto
                if ( !\app\components\Base64Component::toJpeg($request->post('foto'),\app\models\Moto::PATH, $user->moto->foto) ){
                    throw new \Exception('La foto tuvo inconveniente al guardarse');
                }
            }

            if ( !$user->validate() ){
                throw new \Exception(ErrorsComponent::formatJustString($user->errors) );
            }

            $user->moto->save();
            $transaction->commit();
            return $this->formatUser();
        } catch ( \Throwable $e ){
            $transaction->rollBack();
            throw new UnprocessableEntityHttpException( $e->getMessage() );
        }
    }


    private function formatPedido($pedido){
        $detalle = ArrayHelper::toArray($pedido->detalle, [
            PedidoDeliveryDetalle::className() => [
                'id'=>function($model){ return $model->id; },
                'producto_id'=>function($model){ return $model->producto_id; },
                'producto'=>function($model){ 
                    return [
                        'id'=>$model->producto->id,
                        'producto'=>$model->producto->producto,
                        'foto'=>$model->producto->strFoto,
                        'detalle'=>$model->producto->detalle,
                        'costo'=>$model->producto->costo,
                        'estado'=>$model->producto->estado,
                        'categoria_producto_id'=>$model->producto->categoria_producto_id,
                        'categoria_producto'=>$model->producto->categoria,
                    ];
                },
                'subtotal'=>function($model){ return $model->subtotal; },
                'cantidad'=>function($model){ return $model->cantidad; },
                'observacion'=>function($model){ return $model->observacion; },
            ],
        ]);

        return [
            'id'=>$pedido->id,
            'fecha_entrega'=>$pedido->fecha_entrega,
            'hora_entrega'=>$pedido->hora_entrega,
            'fecha_despacho'=>$pedido->asignacionMoto->fecha_despacho,
            'hora_despacho'=>$pedido->asignacionMoto->hora_despacho,
            'fecha_entregado'=>$pedido->asignacionMoto->fecha_entrega,
            'hora_entregado'=>$pedido->asignacionMoto->hora_entrega,

            'cliente_id'=>$pedido->cliente_id,
            'cliente'=>$pedido->cliente,
            'telefono'=>$pedido->telefono,
            'zona'=>$pedido->zona,
            'direccion'=>$pedido->direccion,
            'latitude'=>$pedido->latitude,
            'longitude'=>$pedido->longitude,
            'zoom'=>$pedido->zoom,
            'instrucciones'=>$pedido->instrucciones,
            'precio_delivery_id'=>$pedido->precio_delivery_id,
            'precio_delivery'=>$pedido->precioDelivery,
            'sucursal_delivery_id'=>$pedido->sucursal_delivery_id,
            'sucursal_delivery'=>$pedido->sucursalDelivery,
            'tipo_pedido_id'=>$pedido->tipo_pedido_id,
            'tipo_pedido'=>$pedido->tipoPedido,
            'estado'=>$pedido->estado,
            'strEstado'=>$pedido->strEstado,
            'url_mapa'=>$pedido->url_mapa,
            'detalle'=>$detalle,
            'total'=>$pedido->total,
        ];
    }

    /**
     * Ver pedido
     *
     * muetra los datos del pedido
     * 
     * `Url`: /moto/ver-pedido?id=[id del pedido] <br>
     * `method`: get <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: token <br>
     * @param id integer id del pedido
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "id": 22,
     *             "fecha_entrega": "2020-06-25",
     *             "hora_entrega": "13:13:00",
     *             "fecha_despacho": "2020-06-25",
     *             "hora_despacho": "11:16:00",
     *             "fecha_entregado": null,
     *             "hora_entregado": null,
     *             "cliente_id": 8,
     *             "cliente": {
     *                 "id": 8,
     *                 "nit": "88799999",
     *                 "telefono": "78989989",
     *                 "zona": "villa pagador",
     *                 "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                 "razon_social": "sandra garcia huanaco",
     *                 "zoom": "18",
     *                 "foto": null,
     *                 "latitude": "-17.436701",
     *                 "longitude": "-66.1170413"
     *             },
     *             "telefono": "78989989",
     *             "zona": "villa pagador",
     *             "direccion": "cercca el merado 10 de mayo de la rotonda",
     *             "latitude": "-17.436701",
     *             "longitude": "-66.1170413",
     *             "zoom": "18",
     *             "instrucciones": "esta en esquina frente punto viva",
     *             "precio_delivery_id": 6,
     *             "precio_delivery": {
     *                 "id": 6,
     *                 "nombre": "Costo zona (6)",
     *                 "descripcion": "",
     *                 "precio": "25",
     *                 "estado": "Activo"
     *             },
     *             "sucursal_delivery_id": 4,
     *             "sucursal_delivery": {
     *                 "id": 4,
     *                 "nombre": "SUCURSAL SUD",
     *                 "ciudad": null,
     *                 "detalle": "",
     *                 "estado": "Activo"
     *             },
     *             "tipo_pedido_id": 3,
     *             "tipo_pedido": {
     *                 "id": 3,
     *                 "nombre": "ENTREGA PROGRAMADA",
     *                 "descripcion": "",
     *                 "estado": "Activo"
     *             },
     *             "estado": 1,
     *             "strEstado": "Asignado",
     *             "url_mapa": "https://www.google.com/maps/place/17%C2%B026'12.1%22S+66%C2%B007'01.4%22W/@-17.4366997,-66.1175882,19z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d-17.436701!4d-66.1170413",
     *             "detalle": [
     *                 {
     *                     "id": 20,
     *                     "producto_id": 8,
     *                     "producto": {
     *                         "id": 8,
     *                         "producto": "panchicono",
     *                         "foto": "http://delivery.ds:8000/uploads/productos/producto_8200624160337.jpg",
     *                         "detalle": "fdsfdsfds",
     *                         "costo": "23",
     *                         "estado": "Activo",
     *                         "categoria_producto_id": 3,
     *                         "categoria_producto": {
     *                             "id": 3,
     *                             "nombre": "Comida",
     *                             "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                             "estado": "Activo"
     *                         }
     *                     },
     *                     "subtotal": 46,
     *                     "cantidad": 2,
     *                     "observacion": null
     *                 }
     *             ],
     *             "total": 46
     *         }
     *     }
     */
    public function actionVerPedido($id){
        $pedido = Pedidodelivery::findOne($id);
        if (!$pedido){
            throw new NotFoundHttpException('El pedido no existe');
        }
        return $this->formatPedido($pedido);
    }

    /**
     * Entregar pedido
     *
     * Cambiar de estado al pedido a entregado y 
     * actualizar la  fecha y hora de entregado
     * 
     * `Url`: /moto/entregar-pedido?id=[id del pedido] <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Accept`: application/json <br>
     * `Autorization`: token <br>
     * @param id integer id del pedido
     * @return JSON Ejemplo del resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "id": 22,
     *             "fecha_entrega": "2020-06-25",
     *             "hora_entrega": "13:13:00",
     *             "fecha_despacho": "2020-06-25",
     *             "hora_despacho": "11:16:00",
     *             "fecha_entregado": "2020-06-25",
     *             "hora_entregado": "13:34",
     *             "cliente_id": 8,
     *             "cliente": {
     *                 "id": 8,
     *                 "nit": "88799999",
     *                 "telefono": "78989989",
     *                 "zona": "villa pagador",
     *                 "direccion": "cercca el merado 10 de mayo de la rotonda",
     *                 "razon_social": "sandra garcia huanaco",
     *                 "zoom": "18",
     *                 "foto": null,
     *                 "latitude": "-17.436701",
     *                 "longitude": "-66.1170413"
     *             },
     *             "telefono": "78989989",
     *             "zona": "villa pagador",
     *             "direccion": "cercca el merado 10 de mayo de la rotonda",
     *             "latitude": "-17.436701",
     *             "longitude": "-66.1170413",
     *             "zoom": "18",
     *             "instrucciones": "esta en esquina frente punto viva",
     *             "precio_delivery_id": 6,
     *             "precio_delivery": {
     *                 "id": 6,
     *                 "nombre": "Costo zona (6)",
     *                 "descripcion": "",
     *                 "precio": "25",
     *                 "estado": "Activo"
     *             },
     *             "sucursal_delivery_id": 4,
     *             "sucursal_delivery": {
     *                 "id": 4,
     *                 "nombre": "SUCURSAL SUD",
     *                 "ciudad": null,
     *                 "detalle": "",
     *                 "estado": "Activo"
     *             },
     *             "tipo_pedido_id": 3,
     *             "tipo_pedido": {
     *                 "id": 3,
     *                 "nombre": "ENTREGA PROGRAMADA",
     *                 "descripcion": "",
     *                 "estado": "Activo"
     *             },
     *             "estado": 2,
     *             "strEstado": "Terminado",
     *             "url_mapa": "https://www.google.com/maps/place/17%C2%B026'12.1%22S+66%C2%B007'01.4%22W/@-17.4366997,-66.1175882,19z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d-17.436701!4d-66.1170413",
     *             "detalle": [
     *                 {
     *                     "id": 20,
     *                     "producto_id": 8,
     *                     "producto": {
     *                         "id": 8,
     *                         "producto": "panchicono",
     *                         "foto": "http://delivery.ds:8000/uploads/productos/producto_8200624160337.jpg",
     *                         "detalle": "fdsfdsfds",
     *                         "costo": "23",
     *                         "estado": "Activo",
     *                         "categoria_producto_id": 3,
     *                         "categoria_producto": {
     *                             "id": 3,
     *                             "nombre": "Comida",
     *                             "producto": "Nuestro menu de comida esta compuesto por: Pollo broaster, Alitas etc.",
     *                             "estado": "Activo"
     *                         }
     *                     },
     *                     "subtotal": 46,
     *                     "cantidad": 2,
     *                     "observacion": null
     *                 }
     *             ],
     *             "total": 46
     *         }
     *     }
     */
    public function actionEntregarPedido($id){
        $request = \Yii::$app->request;
        $user = \Yii::$app->user->identity;
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            $pedido = Pedidodelivery::findOne($id);
            if (!$pedido){
                throw new NotFoundHttpException('El pedido no existe');
            }
            $pedido->asignacionMoto->fecha_entrega = Carbon::now()->format('Y-m-d');
            $pedido->asignacionMoto->hora_entrega = Carbon::now()->format('H:i');
            $pedido->estado = Pedidodelivery::ESTADO_TERMINADO;
            if ( !$pedido->asignacionMoto->save() ){
                throw new \Exception(ErrorsComponent::formatJustString($pedido->asignacionMoto->errors) );
            }
            if ( !$pedido->save() ){
                throw new \Exception(ErrorsComponent::formatJustString($pedido->errors) );
            }
            $transaction->commit();
            return  $this->formatPedido($pedido);
        } catch ( \Throwable $e ){
            $transaction->rollBack();
            throw new UnprocessableEntityHttpException( $e->getMessage() );
        }
    }

    /**
     * Cambia la contraseña.
     * 
     * Cambia la contraseña de tipo usuario moto.
     *
     * `Url`: /moto/cambiar-password<br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Autorization`: token <br>
     * `body`: <br>
     *     {
     *         "ChangePassword":{
     *         	   "oldPassword":"123",
     *         	   "newPassword":"123456",
     *         	   "retypePassword":"123456",
     *         }
     *     } 
     * @param int $id id de usuario de la tabla app_user
     * @throws UnprocessableEntityHttpException errore de validacion
     * @return JSON ejemplo de la respuesta:
     *     {
     *         "success": true,
     *         "data": {
     *             "message": "Contraseña fue cambiada correctamente"
     *         }
     *     }
     */
    public function actionCambiarPassword(){
        $model = new ChangePassword();
        $model->scenario = ChangePassword::SCENARIO_USER;

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->change()) {
            return [
                'message'=>'Contraseña fue cambiada correctamente',
            ];
        }

        ErrorsComponent::format($model->errors);
    }

    public function actionCrearClientes() {
        $motos = Moto::find()->all();
        foreach ($motos as $key => $moto) {
            if ($moto->user->cliente_id == null) {
                $cliente = new Cliente();
                $cliente->telefono = $moto->telefono;
                $cliente->razon_social = $moto->nombre;
                $cliente->save();
                $moto->user->cliente_id = $cliente->id;
                $moto->user->save();
            } else {
                $moto->user->cliente->telefono = $moto->telefono;
                $moto->user->cliente->razon_social = $moto->nombre;
                $moto->user->cliente->save();
            }
        }
    }

    /**
     * login mediante sms.
     * 
     * `Url`: /moto/login-phone <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Autorization`: none <br>
     * `body`: <br>
     *     {
     *         "telefono": "78978967",
     *     }
     * @return JSON ejemplo el resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "telefono": "75977665",
     *             "existe": true, //Verifica si la moto existe
     *             "message": "Mensaje enviado"
     *         }
     *     }
     */
    public function actionLoginPhone(){
        $request = \Yii::$app->request;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $login = new LoginPhone();
            $login->telefono = $request->post('telefono');
            if ( !$login->telefono ) {
                throw new \Exception( 'Telefono es requerido' );
            }
            // generar codigo
            $codeRecover = new CodigoTelefono();
            $codeRecover->telefono = $login->telefono;
            $codeRecover->codigo = strval(rand(100000,999999));
            if(!$codeRecover->save()){
                throw new UnprocessableEntityHttpException( ErrorsComponent::formatJustString($codeRecover->errors) );
            }

            // enviando sms
            $enviado = \app\components\SmsComponent::send($codeRecover->telefono, 'Panchita - Tu codigo de acceso es '.$codeRecover->codigo);
            if ($enviado !== true) {
                throw new \Exception($enviado);
            }

            $transaction->commit();
            // verificar si el numero existe (Moto user nueva)
            $exists = Moto::find()->where([
                'telefono'=>trim($codeRecover->telefono)
            ])->exists();

            return [
                'telefono'=>$codeRecover->telefono,
                'existe'=>$exists,
                'message'=>'Mensaje enviado'
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw new UnprocessableEntityHttpException( $e->getMessage() );
        }
    }
    
    /**
     * validar el sms para login phone.
     * 
     * `Url`: /moto/login-phone-confirm <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Autorization`: none <br>
     * `body`: <br>
     *     {
     *         "telefono": "78978967",
     *         "codigo": "8909800",
     *         "player_id": "sdskdhsjhdjshh"
     *         "nombre": "juanito arcoiris", // si la moto es nuevo
     *         "ciudad": "juanito arcoiris", // si la moto es nuevo
     *         "sucursal_id": "avenida los olmos", // si la moto es nuevo
     *     }
     * @return JSON ejemplo el resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "user": {
     *                 "id": 5579,
     *                 "rol": 2,
     *                 "email": "75977665@tempdelivery.com",
     *                 "moto_id": 468,
     *                 "nombre": "willam el delivery",
     *                 "ciudad": "Cochabamba",
     *                 "sucursal_id": 2,
     *                 "sucursal": {
     *                     "id": 2,
     *                     "nombre": "SUCURSAL VILLAZON",
     *                     "ciudad": "Cochabamba",
     *                     "detalle": "FERCA S.R.L.",
     *                     "estado": "Activo",
     *                     "latitude": "-17.37490479701525",
     *                     "longitude": "-66.13537526394207"
     *                 },
     *                 "telefono": "75977665",
     *                 "foto": null,
     *                 "estado": 0,
     *                 "player_id": "897897897979879"
     *             },
     *             "access_token": "_6AP3WKrCJCSlXpp-X770W-LsPC7qUBT"
     *         }
     *     }
     */
    public function actionLoginPhoneConfirm(){
        $request = \Yii::$app->request;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $login = new LoginPhone();
            $login->telefono = $request->post('telefono');
            $codigo = $request->post('codigo');
            if ( !$login->telefono ) {
                throw new \Exception('El telefono es requerido');
            }
            // validando el codigo
            $codigoTelefono = CodigoTelefono::findOne([
                'codigo'=>trim($codigo),
                'telefono'=>$login->telefono,
                'valido'=>1
            ]);
            if( !$codigoTelefono ){
                throw new UnprocessableEntityHttpException( 'El codigo es incorrecto' );
            }
            // descartando codigo
            $codigoTelefono->valido = 0;
            if(!$codigoTelefono->save()){
                throw new UnprocessableEntityHttpException( ErrorsComponent::formatJustString($codigoTelefono->errors) );
            }

            // verificando si la moto existe (Moto nueva)
            $motoExist = Moto::find()->where([
                'telefono'=>$login->telefono
            ])->exists();
            if (!$motoExist) {
                $moto = new Moto();
                $moto->telefono = $request->post('telefono');
                $moto->nombre = $request->post('nombre');
                $moto->ciudad = $request->post('ciudad');
                $moto->sucursal_id = $request->post('sucursal_id');
                $moto->player_id = $request->post('player_id');
                if(!$moto->save()){
                    throw new \Exception( ErrorsComponent::formatJustString($moto->errors) );
                }
                $user = new SignupForm();
                $user->email = $moto->telefono.'@tempdelivery.com'; // email temporal
                $user->password_hash = $moto->telefono;
                $user->moto_id = $moto->id;
                if (!$user->validate()){
                    throw new \Exception( ErrorsComponent::formatJustString($user->errors) );
                }
                $user = $user->signup();
                if (!$user){
                    throw new \Exception( 'Ocurrio algo al registrar usuario' );
                }
                $transaction->commit();
                return [
                    'user'=>$this->formatUser($user->id),
                    'access_token'=>$user->access_token
                ];
            }

            // Verificando si la moto no tiene un usuario (Moto sin user)
            $moto = Moto::findOne(['telefono'=>$codigoTelefono->telefono]);
            if (!$moto->user) {
                $user = new SignupForm();
                $user->email = $moto->telefono.'@tempdelivery.com'; // email temporal
                $user->password_hash = $moto->telefono;
                $user->moto_id = $moto->id;
                if (!$user->validate()){
                    throw new \Exception( ErrorsComponent::formatJustString($user->errors) );
                }
                $user = $user->signup();
                if (!$user){
                    throw new \Exception( 'Ocurrio algo al registrar usuario' );
                }
                $transaction->commit();
                return [
                    'user'=>$this->formatUser($user->id),
                    'access_token'=>$user->access_token
                ];
            }

            // login normal con moto y user existente (Moto existente)
            if ( $login->login() ) {
                $login->user->moto->player_id = $request->post('player_id');
                $login->user->moto->save();
                $transaction->commit();
                return [
                    'user'=>$this->formatUser($login->user->id),
                    'access_token'=>$login->user->access_token
                ];
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw new UnprocessableEntityHttpException( $e->getMessage() );
        }
    }

    /**
     * Modificar los datos de moto.
     * inluyendo la el id de la sucursal a la que pertenece
     * 
     * `Url`: /moto/update <br>
     * `method`: post <br>
     * `Content-Type`: application/json <br>
     * `Autorization`: none <br>
     * `body`: <br>
     *     {
     *         "sucursal_id": "2",
     *     }
     * @return JSON ejemplo el resultado:
     *     {
     *         "success": true,
     *         "data": {
     *             "id": 5580,
     *             "rol": 2,
     *             "email": "75977665@tempdelivery.com",
     *             "moto_id": 468,
     *             "nombre": "willam el delivery",
     *             "ciudad": "Cochabamba",
     *             "sucursal_id": "2",
     *             "sucursal": {
     *                 "id": 2,
     *                 "nombre": "SUCURSAL VILLAZON",
     *                 "ciudad": "Cochabamba",
     *                 "detalle": "FERCA S.R.L.",
     *                 "estado": "Activo",
     *                 "latitude": "-17.37490479701525",
     *                 "longitude": "-66.13537526394207"
     *             },
     *             "telefono": "75977665",
     *             "foto": null,
     *             "estado": 0,
     *             "player_id": "897897897979879"
     *         }
     *     }
     */
    public function actionUpdate() {
        $request = \Yii::$app->request;
        $user = \Yii::$app->user->identity;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ( !$request->post('sucursal_id') ){
                throw new \Exception( 'La sucursal es requerida' );
            }
            if ( !$user->moto ){
                throw new \Exception('El usuario no tiene datos de moto');
            }
            $user->moto->sucursal_id = $request->post('sucursal_id');

            if ( !$user->moto->save() ){
                throw new \Exception(ErrorsComponent::formatJustString($user->moto->errors) );
            }
            $transaction->commit();
            return $this->formatUser();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw new UnprocessableEntityHttpException( $e->getMessage() );
        }
    }

    /**
     * Lista de sucursales.
     * 
     * `Url`: /moto/sucursales <br>
     * `method`: get <br>
     * `Content-Type`: application/json <br>
     * `Autorization`: Token <br>
     * `body`: <br>
     * @return JSON ejemplo el resultado:
     *     {
     *         "success": true,
     *         "data": [
     *             {
     *                 "id": 2,
     *                 "nombre": "SUCURSAL VILLAZON",
     *                 "ciudad": "Cochabamba",
     *                 "detalle": "FERCA S.R.L.",
     *                 "estado": "Activo",
     *                 "latitude": "-17.37490479701525",
     *                 "longitude": "-66.13537526394207"
     *             },
     *             {
     *                 "id": 3,
     *                 "nombre": "SUCURSAL BLANCO GALINDO",
     *                 "ciudad": "Cochabamba",
     *                 "detalle": "FERCA S.R.L.",
     *                 "estado": "Activo",
     *                 "latitude": "-17.392490456145595",
     *                 "longitude": "-66.197140778017"
     *             },
     *         ]
     *     }
     */
    public function actionSucursales() {
        return Sucursaldelivery::find()->all();
    }

}