<?php 
namespace app\controllers;

use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\Signup;
use app\models\User;
use app\models\Login;
use app\models\ChangePassword;
use app\models\Producto;
use app\models\Favorito;
use app\models\Solicitud;
use app\models\Calificacion;
use app\models\LoginGmail;
use app\models\SignupGmail;
use app\models\LoginFacebook;
use app\models\SignupFacebook;

use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\BadRequestHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UnprocessableEntityHttpException;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

use app\components\errors\ErrorsComponent;

/**
 * Controlador de usuario para autentificaciones y registro de usuario interesado.
 * 
 * Mediante ete controlador se puede autentificar usuarios de tipo estudiante o 
 * interesado, tambien se pude registrar usuarios de tipo interesado.<br>
 * Usa el tipo de autentificación `Bearer Token` para autorizar la respuesta de un 
 * determinado recurso. Tambien la forma de respuesta se efectua en formato JSON
 * de la siguiente manera:<br>
 *     {
 *         "success":true|false,
 *         "data":{
 *              [RESULTADO]
 *         }
 *     }
 * Donde el atributo `success` es de tipo boolean para deternimar si un recurso es satisfactorio 
 * o falso si se ha tenido algun inconveniente como acceso no autorizado, validacion, etc.
 * y el atributo `data` es de tipo Mixto es donde se almacena el resultado de cada recuro que se 
 * quiera solicitar.<br>
 * En caso de ocurrir una excepcion el resutlado dara un success false y un data con un mensaje, nombre
 * de la excepcion y el codigo de error, teniendo un 
 * contenido similar a este ejemplo:<br>
 *     {
 *         "success": false,
 *         "data": {
 *             "name": "Unauthorized",
 *             "message": "El Estudiante no es valido",
 *             "code": 0,
 *             "status": 401,
 *             "type": "yii\\web\\UnauthorizedHttpException"
 *         }
 *     }
 * 
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'corsFilter'  => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
            'bearerAuth' => [
                'class' => HttpBearerAuth::className(),
                'except'=>[
                    'index',
                ]
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => [
                            'index', 
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Accion de la pagina principal
     * @return JSON
     */
    public function actionIndex()
    {
        return "Welcome to API's CAMBALACHE";
    }
}