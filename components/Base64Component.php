<?php
namespace app\components;

use Yii;
use yii\web\UnprocessableEntityHttpException;

/**
 * Componente para convertir el array de model->errors a una 
 * cadena pqra que e pueda visualizar en un mensaje string
 */
class Base64Component 
{
    /**
     * convierte un string en imagen
     * 
     * Codifica una string en base 64 para guardar una imagen.
     * Codifica una cadena decodificada en base 64 para generar
     * un archivo con la extencion que tenga el nombre de la 
     * variable $output_file y guardarlo en la dirección 
     * /uploads/interesados/fotoperfil/
     * @param string $base64_string
     * @param string $output_file
     * @return int|false devuelve el número de bytes escritos, 
     * o FALSE si se produjo un error.
     */
    static public function toJpeg($base64_string, $directory, $output_file) {
        // open the output file for writing
        $ifp = fopen( \Yii::getAlias('@imagePath').$directory.$output_file, 'wb' ); 
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
        if(count($data)>1) {
            $dataText=$data[ 1 ];
        } else {
            $dataText=$base64_string;
        }
        // we could add validation here with ensuring count( $data ) > 1
        $res = fwrite( $ifp, base64_decode( $dataText ) );
        // clean up the file resource
        fclose( $ifp ); 
        return $res; 
    }

}
