<?php
namespace app\components;

use Yii;
use yii\web\UnprocessableEntityHttpException;
use yii\web\UnauthorizedHttpException;
use Twilio\Rest\Client;

/**
 * Componente para enviar sms mediante twillio api
 */
class SmsComponent 
{
    // Your Account SID and Auth Token from twilio.com/console
    const sid = 'ACe3176336c9d58f5178eb873a7a7c2437';
    const token = 'c2206a47275cd5f05ed4289fc34ecc0c';
    const from = '+13862226928';

    /**
     * envia el sms para login telefono
     *
     * @throws UnauthorizedHttpException
     * @return app\models\Estudiante
     */
    static public function send($telefono, $message) {
        try {
            $client = new Client(self::sid, self::token);
    
            // Use the client to do fun stuff like send text messages!
            $responce = $client->messages->create(
                // the number you'd like to send the message to
                '+591'.$telefono,
                [
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => self::from,
                    // the body of the text message you'd like to send
                    'body' => $message
                ]
            );
            return true;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

}
